"""
b = {}
b[4] = [1, 2]
b[6] = [2, 5]
b[8] = [7, 14]

with open("test.txt", "w") as out:
    for i in b:
        out.write(str(i) + " " + str(b[i][0]) + " " + str(b[i][1]))
        out.write("\n")
"""

c = {}
with open("test.txt", "r") as info:
    loopcontrol = 3
    while loopcontrol > 0:
        text = info.readline()
        text = text.split()
        c[int(text[0])] = [int(text[1]), int(text[2])]
        loopcontrol-=1

print(c)