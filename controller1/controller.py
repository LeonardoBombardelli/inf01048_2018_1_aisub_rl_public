import interfaces as controller_template
from itertools import product
from typing import Tuple, List, Any
import random
import math

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)

class ActionQValue:
    def __init__(self):
        self.stayValue = random.uniform(0, 1)
        self.goUpValue = random.uniform(0, 1)


class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors

        #Comments with CHANGE are things that can be interesting to change. Look closer to that when we can test things.

        self.lastState              = [0, 0, 0, 0, 0, 0, 0]

        self.obstacleUp             = [-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
        self.obstacleUpRight        = [-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
        self.obstacleDownRight      = [-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
        self.obstacleDown           = [-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
        self.obstacleAhead          = -1.0  # Unlike other values, we don't know how fast the ship goes up or down

        self.waterUp                = -1.0
        self.lastOxigenValue        = -1.0

        self.recoveringOxygen       = False

        self.nextReadingIndex       = 0
        

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the submarine (see below) and compute useful features for selecting an action
        The submarine has the following sensors:

        self.sensors contains (in order):
        
        0 water_UP: 1-700
        1 water_UP_RIGHT: 1-700
        2 obstacle_UP: 1-700
        3 obstacle_UP_RIGHT: 1-700
        4 obstacle_AHEAD: 1-700
        5 obstacle_DOWN_RIGHT: 1-700
        6 obstacle_DOWN: 1-700
        7 monster_UP: 1-200
        8 monster_UP_RIGHT: 1-200
        9 monster_AHEAD: 1-200
        10 monster_DOWN_RIGHT: 1-200
        11 monster_DOWN: 1-200
        12 oxygen: 1-400        
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """
        
        nri = self.nextReadingIndex

        self.obstacleUp[nri]        = min(self.sensors[2], self.sensors[7])     #obstacle_UP vs monster_UP
        self.obstacleUpRight[nri]   = min(self.sensors[3], self.sensors[8])     #obstacle_UP_RIGHT vs monster_UP_RIGHT
        self.obstacleAhead          = min(self.sensors[4], self.sensors[9])     #obstacle_AHEAD vs monster_AHEAD
        self.obstacleDownRight[nri] = min(self.sensors[5], self.sensors[10])    #obstacle_DOWN_RIGHT vs monster_DOWN_RIGHT
        self.obstacleDown[nri]      = min(self.sensors[6], self.sensors[11])    #obstacle_DOWN vs monster_DOWN

        self.nextReadingIndex += 1
        if self.nextReadingIndex == len(self.obstacleUp):
            self.nextReadingIndex = 0
        
        oxygenDifference = self.sensors[12] - self.lastOxigenValue   #If this = -1, normal behavior; = -2, got caught by monster; positive value: recovering oxygen
        self.lastOxigenValue = self.sensors[12]

        oxygenUp = self.sensors[0]

        avoidUpperObstacle          = min(self.obstacleUp)
        avoidUpperRightObstacle     = min(self.obstacleUpRight)
        avoidAhead                  = self.obstacleAhead
        avoidBottomRightObstacle    = min(self.obstacleDownRight)
        avoidBottomObstacle         = min(self.obstacleDown)

        results = [0, 0, 0, 0, 0, 0, 0]
        results[0] = oxygenUp
        results[1] = oxygenDifference
        results[2] = avoidUpperObstacle
        results[3] = avoidUpperRightObstacle
        results[4] = avoidAhead
        results[5] = avoidBottomRightObstacle
        results[6] = avoidBottomObstacle

        return results


    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """

        #My actual logic: We want our sub to be as low on the ground as he can
        #Why? Because it's easier and quicker for it to go up once an obstacle appears
        #With that in mind, our discretization levels for obstacleBottom and obstacleUp should be different
        #I don't know if it will make any impact, but I hope it will provide the sub with the information to work with
        #Wish me luck :)

        oxygenUp                    = features[0]   #Two states: = 700 -> no oxygen up; < 700 -> oxygen up
        oxygenDifference            = features[1]   #Two states: = 0 -> is losing oxygen; = 1 -> is getting oxygen
        avoidUpperObstacle          = features[2]   #Goes up 40 each up  action; 3 threshholds: <= 50; <= 120; > 120; we can CHANGE the value, to just 2 values, maybe 
        avoidUpperRightObstacle     = features[3]   #<= 100; <= 190; > 190
        avoidAhead                  = features[4]   #<= 60; <= 150; > 150
        avoidBottomRightObstacle    = features[5]   #<= 50; <= 150; > 150 CHANGE
        avoidBottomObstacle         = features[6]   #<= 40; <= 120; > 120

        if oxygenUp == 700:
            oxygenUp = 0
        else:
            oxygenUp = 1
        

        if oxygenDifference < 0:   
            oxygenDifference = 0
            self.recoveringOxygen = False
        else:
            oxygenDifference = 1
            self.recoveringOxygen = True


        if avoidUpperObstacle <= 50:
            avoidUpperObstacle = 0
        elif avoidUpperObstacle <= 120:
            avoidUpperObstacle = 1
        else:
            avoidUpperObstacle = 2
        

        if avoidUpperRightObstacle <= 100:
            avoidUpperRightObstacle = 0
        elif avoidUpperRightObstacle <= 190:
            avoidUpperRightObstacle = 1
        else:
            avoidUpperRightObstacle = 2

        if avoidAhead <= 60:
            avoidAhead = 0
        elif avoidAhead <= 150:
            avoidAhead = 1
        else:
            avoidAhead = 2


        if avoidBottomRightObstacle <= 50:
            avoidBottomRightObstacle = 0
        elif avoidBottomRightObstacle <= 150:
            avoidBottomRightObstacle = 1
        else:
            avoidBottomRightObstacle = 2


        if avoidBottomObstacle <= 40:
            avoidBottomObstacle = 0
        elif avoidBottomObstacle <= 120:
            avoidBottomObstacle = 1
        else:
            avoidBottomObstacle = 2

        self.lastState = [oxygenUp, oxygenDifference, avoidUpperObstacle, avoidUpperRightObstacle, avoidAhead, avoidBottomRightObstacle, avoidBottomObstacle]

        return [oxygenUp, oxygenDifference, avoidUpperObstacle, avoidUpperRightObstacle, avoidAhead, avoidBottomRightObstacle, avoidBottomObstacle]

    #CHANGE
    @staticmethod
    def stateToString(feats: Tuple) -> str:
        """
        Receives the discretized features, returns a string that will be used to the dict key
        """
        returnStr = ""
        for i in feats:
            returnStr+= str(i)
        return returnStr

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """

        oxygenUpLevels = 2
        oxygenDifferenceLevels = 2
        avoidUpperObstacleLevels = 3
        avoidUpperRightObstacleLevels = 3
        avoidAheadLevels = 3
        avoidBottomRightObstacleLevels = 3
        avoidBottomObstacleLevels = 3
        return [oxygenUpLevels, oxygenDifferenceLevels, avoidUpperObstacleLevels, avoidUpperRightObstacleLevels, avoidAheadLevels, avoidBottomRightObstacleLevels, avoidBottomObstacleLevels]



    def get_current_state(self):
        """
        :return: computes the discretized features associated with this state object.
        """
        features = self.discretize_features(self.compute_features())
        return features

    @staticmethod
    def get_state_id(discretized_features: Tuple) -> int:
        """
        Handy function that calculates an unique integer identifier associated with the discretized state passed as
        parameter.
        :param discretized_features
        :return: unique key
        """

        terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

        s_id = 1
        for i in range(len(discretized_features)):
            s_id = s_id * terms[i]

        return s_id

    @staticmethod
    def get_number_of_states() -> int:
        """
        Handy function that computes the total number of possible states that exist in the system, according to the
        discretization levels specified by the user.
        :return:
        """
        v = State.discretization_levels()
        num = 1

        for i in (v):
            num *= i

        return num

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]



class QTable(controller_template.QTable):
    def __init__(self):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        
        self.qValues = {}
        
        for i in State.enumerate_all_possible_states():
            self.qValues[State.stateToString(i)] = ActionQValue()
        

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object 
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        
        stateKey = State.stateToString(key.lastState)
        
        if action == 0:
            return self.qValues[stateKey].stayValue
        else:
            return self.qValues[stateKey].goUpValue



    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object 
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return: 
        """
        
        stateKey = State.stateToString(key.lastState)

        if action == 0:
            self.qValues[stateKey].stayValue = new_q_value
        else:
            self.qValues[stateKey].goUpValue = new_q_value

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path: 
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """

        #TODO: Use save and load in the execution

        with open(path, "a") as out:
            for i in self.qValues:
                out.write(str(i) + " " + str(self.qValues[i].stayValue) + " " + str(self.qValues[i].goUpValue))
                out.write("\n")

    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        with open(path, "r") as info:
            for _ in range(0, State.get_number_of_states()):
                text = info.readline()
                text = text.split()

                qvalue = QTable()
                qvalue.qValues[str(text[0])].stayValue = float(text[1])
                qvalue.qValues[str(text[0])].goUpValue = float(text[2])

                return qvalue


class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_episode: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get in new_state
        :param n_steps: number of steps the sub has taken so far in the current race
        :param end_of_episode: boolean indicating if an episode has ended
        :return: The reward to be given to the agent
        """
        reward = 0

        #discretize_features: [oxygenUp, oxygenDifference, avoidUpperObstacle, avoidUpperRightObstacle, avoidAhead, avoidBottomRightObstacle, avoidBottomObstacle]
        discretizedFeatures = new_state.lastState     #CHANGE

        if end_of_episode == True:
            reward -= 50.00
        
        if new_state.recoveringOxygen == True:
            reward += 1.0

        if discretizedFeatures[6] == 1: #CHANGE, maybe. Just to test if he can be close to the ground but not too close
            reward += 0.1

        if discretizedFeatures[5] == 1:
            reward += 0.1
        
        if new_state.sensors[12] == new_state.lastOxigenValue - 2: #Touching monster
            reward -= 0.5
        
        return reward


    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get to new_state
        :param reward: the reward the sub received for getting to new_state  
        :param end_of_episode: boolean indicating if an episode has ended
        """

        newStateKey = State.stateToString(new_state.lastState)
        oldStateKey = State.stateToString(old_state.lastState)

        alpha = 0.3
        gamma = 0.6

        QValuesOfNewState = self.q_table.qValues[newStateKey]
        maxQValueNewState = max(QValuesOfNewState.stayValue, QValuesOfNewState.goUpValue)

        computedValue = alpha * (reward + gamma * maxQValueNewState)

        if action == 0:
            actualValue = self.q_table.qValues[oldStateKey].stayValue
        else:
            actualValue = self.q_table.qValues[oldStateKey].goUpValue
        
        newValue = ((1 - alpha) * actualValue) + computedValue

        if action == 0:
            self.q_table.qValues[oldStateKey].stayValue = newValue
        else:
            self.q_table.qValues[oldStateKey].goUpValue = newValue



    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the submarine must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the submarine 
        :param episode_number: current episode/race during the training period
        :return: The action the submarine chooses to execute
        """

        #TODO: Make a more meaningful exploratory condition

        newStateKey = State.stateToString(new_state.lastState)

        e = 0.05

        if random.uniform(0, 1) < e:
            i = random.uniform(0, 1)
            if i > 0.5:
                return 1
            else:
                return 0
        else:
            if self.q_table.qValues[newStateKey].goUpValue > self.q_table.qValues[newStateKey].stayValue:
                return 1
            else:
                return 0 

        #if 50 % episode_number  == 0:
        #    self.q_table.save("Aaa.txt")